package chess;
import java.util.ArrayList;
import java.util.Scanner;

import gamePieces.Queen;

/**
 * Chess creates and runs a game of chess
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 */
public class Chess {

	/**
	 * Prints whose turn it is given the turn counter
	 * @param whiteTurn boolean that represents whether or not it is white's turn
	 * 
	 */
	public static void printTurn(boolean whiteTurn) {
		if (whiteTurn) {
			System.out.print("White's move: ");
		} else {
			System.out.print("Black's move: ");
		}
	}

	/**
	 * Returns whether or not the opposing king is safe, in check, or in mate given the board and whose turn it is.
	 * @param board The board being played on
	 * @param whiteTurn Boolean representing whose turn it is
	 * @return 0 for safe, 1 for in check, and 2 for in mate
	 * 
	 */
	public static int checkKing(GameBoard board, boolean whiteTurn) {
		ArrayList<int[]> attackers = new ArrayList<int[]>();
		int[] king = {0, 0};
		
		//this loop establishes the king and attackers (all enemy pieces relative to the established king)
		for(int y = 0; y < 8; y++) {
			for (int x = 0; x < 8; x++) {
				if(board.board[y][x] != null) {
					if((board.board[y][x].print().equals("bK") && whiteTurn == true) || (board.board[y][x].print().equals("wK") && whiteTurn == false)) {
						king[0] = y;
						king[1] = x;
					}
					if((whiteTurn == true && board.board[y][x].getColor() == false) || (whiteTurn == false && board.board[y][x].getColor() == true)) {
						int[] attacker = {y, x};
						attackers.add(attacker);
					}
				}
			}
		}
		
		for(int i = 0; i < attackers.size(); i++) {
			int[] atkr = attackers.get(i);
			int[] coords = {atkr[0], atkr[1], king[0], king[1]};
			if(board.board[atkr[0]][atkr[1]].move(board, coords)) {
				if(checkmate(board, king, attackers)) {
					return 2;
				}
				return 1;
			}
		}
		
		return 0;
	}
	
	/**
	 * Returns true or false whether or not all of the empty spaces around the king can be attacked by opposing pieces.
	 * @param board The board being played on
	 * @param king An int array holding the coordinate of the king in [file rank]
	 * @param attackers An ArrayList of [file rank] coordinates of all opposing pieces
	 * @return True if moving king to all adjacent spaces still results in check
	 * 
	 */
	public static boolean checkmate(GameBoard board, int[] king, ArrayList<int[]> attackers) {
		ArrayList<int[]> emptySpaces = new ArrayList<int[]>();
		int[] coordinates = {0, 0, 0, 0};
		int[] atkr;
		int[] empty;
		int attacks = 0;
		
		//retrieves coordinates of empty spaces around the king
		for(int i = king[0] - 1 ; i < king[0] + 2; i++) {
			for(int j = king[1] - 1; j < king[1] + 2; j++) {
				if(i > 0 && j > 0 && i < 7 && j < 7) {
					if(board.board[i][j] == null) {
						int[] espace = {i, j};
						emptySpaces.add(espace);
					}
				}
			}
		}
		
		for(int x = 0; x < emptySpaces.size(); x++) {
			empty = emptySpaces.get(x);
			for(int y = 0; y < attackers.size(); y++) {
				atkr = attackers.get(y);
				
				coordinates[0] = atkr[0];
				coordinates[1] = atkr[1];
				coordinates[2] = empty[0];
				coordinates[3] = empty[1];
				
				if(board.board[atkr[0]][atkr[1]].move(board, coordinates)) {
					attacks += 1;
				}
			}
		}
		
		if(attacks == emptySpaces.size()) {
			return true;
		} else {
			return false;
		}
	}
	
/**
 * Returns whether or not a castle was performed
 * @param board Board being played on
 * @param input An int array containing the move set (start and end coordinates on the board)
 * @param whiteTurn Boolean representing whose turn it is
 * @return True if the move is a castle
 */
	public static boolean checkCastle(GameBoard board, int[] input, boolean whiteTurn) {
			int numEmpty = 0;
			
			//if right castle
			if(input[0]-input[2] < 0) {
				//see if king's moves are interrupted by check
				int i = 1;
				for(i = 1; i < 3; i++) {
					GameBoard testBoard = new GameBoard();
					testBoard.board = board.board;
				
					testBoard.board[input[0] + i][input[1]] = testBoard.board[input[0] + i -1][input[1]];
					testBoard.board[input[0] + i -1][input[1]] = null;
					
					if(checkKing(board, !whiteTurn) != 0) {
						break;
					}
					
					numEmpty += 1;
				}
				
				//restore board
				board.board[input[0]][input[1]] = board.board[input[0] + i -1][input[1]];
				board.board[input[0] + i -1][input[1]] = null;
				
				//castle (move rook only)
				if(numEmpty == 2) {
					
					board.board[input[0] +1][input[1]] = board.board[7][input[1]];
					board.board[7][input[1]] = null;
					
					return true;
				} else {
					return false;
				}
			}
			//else left castle
			else {
				int i = 1;
				for(i = 1; i < 3; i++) {
					GameBoard testBoard = new GameBoard();
					testBoard.board = board.board;
					
					testBoard.board[input[0] - i][input[1]] = testBoard.board[input[0] -i +1][input[1]];
					testBoard.board[input[0] - i +1][input[1]] = null;
					
					if(checkKing(testBoard, !whiteTurn) != 0) {
						break;
					}
					
					numEmpty += 1;
				}
				
				//restore board
				board.board[input[0]][input[1]] = board.board[input[0] - i +1][input[1]];
				board.board[input[0] - i +1][input[1]] = null;
				
				//castle (move rook only)
				if(numEmpty == 2) {

					board.board[input[0] - 1][input[1]] = board.board[0][input[1]];
					board.board[0][input[1]] = null;
					
					return true;
				} else {
					return false;
				}
			}
	}
	
	/**
	 * Returns whether or not the king will be in check with the next move
	 * @param board Board being played on
	 * @param input An int array containing the move set (start and end coordinates on the board)
	 * @return False if the move will put the king into check
	 */
	public static boolean nextCheck(GameBoard board, int[] input) {
		boolean validMove = true;
		boolean whiteTurn;
		//boolean whiteTarget;
		
		//establish whose turn it is
		if(board.board[input[0]][input[1]].getColor() == false) {
			whiteTurn = true;
		} else {
			whiteTurn = false;
		}
		
		//move piece on the board
		board.board[input[2]][input[3]] = board.board[input[0]][input[1]];
		board.board[input[0]][input[1]] = null;
		
		if(checkKing(board, !whiteTurn) == 1) {
			validMove = false;
		}
		
		//restore board
		board.board[input[0]][input[1]] = board.board[input[2]][input[3]];
		board.board[input[2]][input[3]] = null;
		
		return validMove;
	}
	
	/**
	 * Runs chess game
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		
		//this is a test function that prints board and allows for moving pieces around
		//no game functionality such as turns or check/checkmate
	
		//start game with white's turn
		boolean whiteTurn = true;
				
		boolean validMove = true;
		GameBoard board = new GameBoard();
		board.initializeBoard();
		
		int[] input;
		
		Scanner in = new Scanner(System.in);
		
		while(true) {
			board.printBoard();
			
			System.out.println();
			printTurn(whiteTurn);
			
			do {
				input = InputReader.inputReader(in);
				
				System.out.println();
				
				//System.out.println(input[0] + ", " + input[1] + ", " + input[2] + ", " + input[3]); //for debugging
				
				//if resign
				if(input.length == 1 && input[0] == 99) {
					if(!whiteTurn) {
						System.out.println("White wins");
					} else {
						System.out.println("Black wins");
					}
					return;
				} else if(input.length ==1) {
					System.out.println("illegal move, try again \n");
					printTurn(whiteTurn);
					validMove = false;
					continue;
				}
				
				//starting piece is empty
				if(board.board[input[0]][input[1]] == null) {
					System.out.println("illegal move, try again \n");
					printTurn(whiteTurn);
					validMove = false;
				}
				//moving opponent's piece
				else if((board.board[input[0]][input[1]].getColor() && whiteTurn) || (!board.board[input[0]][input[1]].getColor() && !whiteTurn)) {
					System.out.println("illegal move, try again \n");
					printTurn(whiteTurn);
					validMove = false;
				}
				//valid move
				else if(board.board[input[0]][input[1]].move(board, input)) {
					//check for draw
					if(input.length == 5) {
						if(input[4] == -99) {
							System.out.println("Draw");
							return;
						}
					}
					
					//check if next move will cause check
					if(!nextCheck(board,input)) {
						System.out.println("illegal move, try again \n");
						printTurn(whiteTurn);
						validMove = false;
						continue;
					}
					
					//check for castle
					if(board.board[input[0]][input[1]].print().charAt(1) == 'K' && Math.abs(input[0] - input[2]) == 2) {
						if(!checkCastle(board, input, whiteTurn)) {
							System.out.println("illegal move, try again \n");
							printTurn(whiteTurn);
							validMove = false;
							continue;
						};
					}
					
					//check for promotion
					if(board.board[input[0]][input[1]].print().charAt(1) == 'p' && input.length == 5 && (input[3] == 0 || input[3] == 7)) {
						board.board[input[2]][input[3]] = InputReader.getPiece(input[4], whiteTurn);
						board.board[input[0]][input[1]] = null;
					}
					else if(board.board[input[0]][input[1]].print().charAt(1) == 'p' && (input[3] == 0 || input[3] == 7)) {
						board.board[input[2]][input[3]] = new Queen(!whiteTurn);
						board.board[input[0]][input[1]] = null;
					}
					else {
						//move piece on the board
						board.board[input[2]][input[3]] = board.board[input[0]][input[1]];
						board.board[input[0]][input[1]] = null;
					}
					
					if(checkKing(board, whiteTurn) == 2) { 
						System.out.println("Checkmate");
						if(whiteTurn) {
							System.out.println("White wins");
						} else {
							System.out.println("Black wins");
						}
						return; 
					}
					else if(checkKing(board, whiteTurn) == 1) {
						System.out.println("\nCheck");
					}

					
					whiteTurn = !whiteTurn;
					validMove = true;
				}
				else {
					System.out.println("illegal move, try again \n");
					printTurn(whiteTurn);
					validMove = false;
				}
			} while (validMove == false);
			
		}
		
	}
}
