package chess;

import java.util.Scanner;
import java.util.StringTokenizer;

import gamePieces.Bishop;
import gamePieces.Knight;
import gamePieces.Pawn;
import gamePieces.Piece;
import gamePieces.Queen;
import gamePieces.Rook;


/**
 * InputReader contains the methods necessary to read and parse inputs to the chess game
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class InputReader {
	
	/**
	 * Reads user input and returns an integer array of coordinates 
	 * @param in User input
	 * @return integer array of user inputed coordinates
	 */
	public static int[] inputReader(Scanner in) {
		
		//Scanner in = new Scanner(System.in);
		String input = in.nextLine();
		StringTokenizer st = new StringTokenizer(input);
		
		//starting coordinates
		String inputToken = st.nextToken();
		
		//if input is 1 word
		if (st.countTokens() == 0) {
			
			//check for resign
			if(inputToken.equals("resign")) {
				int[] coords = {99};
				return coords;
			} 
			//check for other words/typo
			else {
				int[] coords = {101};
				return coords;
			}
		}
		
		int coord1 = convertCoord(inputToken.substring(0,1));
		int coord2 = 8 - Integer.parseInt(inputToken.substring(1,2));
		
		//ending coordinates
		inputToken = st.nextToken();
		int coord3 = convertCoord(inputToken.substring(0,1));
		int coord4 = 8 - Integer.parseInt(inputToken.substring(1,2));
		
		//user input only had 2 arguments
		if(!st.hasMoreTokens()) {
			int[] coords = {coord1, coord2, coord3, coord4};
			return coords;
		}
		else {
			inputToken = st.nextToken();
			int[] coords = {coord1, coord2, coord3, coord4, -1};
			if(inputToken.equals("draw?")) {
				coords[4] = -99;
			} else {
				int piece = convertPiece(inputToken.substring(0,1));
				coords[4] = piece; //maybe swap order
			}
			return coords;
			
		}
		
		
	}
	
	
	/**
	 * Converts chess rank into integer coordinate
	 * @param coord string representing rank on a chess board
	 * @return integer coordinate corresponding to given rank
	 * 
	 */
	public static int convertCoord(String coord) {
		
		if(coord.equals("a")) {
			return 0;
		}
		else if(coord.equals("b")) {
			return 1;
		}
		else if(coord.equals("c")) {
			return 2;
		}
		else if(coord.equals("d")) {
			return 3;
		}
		else if(coord.equals("e")) {
			return 4;
		}
		else if(coord.equals("f")) {
			return 5;
		}
		else if(coord.equals("g")) {
			return 6;
		}
		else if(coord.equals("h")) {
			return 7;
		}
		
		return -1;
		
	}
	
	/**
	 * Converts piece desired during promotion into an integer token
	 * @param piece string representing desired piece
	 * @return integer token that corresponds to desired piece
	 * 
	 */
	public static int convertPiece(String piece) {
		
		if(piece.equals("p")) {
			return 0;
		}
		else if(piece.equals("R")) {
			return 1;
		}
		else if(piece.equals("N")) {
			return 2;
		}
		else if(piece.equals("B")) {
			return 3;
		}
		else if(piece.equals("Q")) {
			return 4;
		}
		
		return -1;
		
	}
	
	/**
	 * takes an integer token and returns a piece of the corresponding type
	 * @param token integer token representing piece
	 * @param whiteTurn whether the current turn is white
	 * @return piece object of the correct type
	 * 
	 */
	public static Piece getPiece(int token, boolean whiteTurn) {
		boolean blackTurn = !whiteTurn;
		if(token == 0) {
			return new Pawn(blackTurn);
		}
		else if(token == 1) {
			return new Rook(blackTurn);
		}
		else if(token == 2) {
			return new  Knight(blackTurn);
		}
		else if(token == 3) {
			return new Bishop(blackTurn);
		}
		else if(token == 4) {
			return new Queen(blackTurn);
		}
		
		return null;
	}

}
