package chess;

import gamePieces.Bishop;
import gamePieces.King;
import gamePieces.Knight;
import gamePieces.Pawn;
import gamePieces.Piece;
import gamePieces.Queen;
import gamePieces.Rook;

/**
 * GameBoard uses a 2d array of Pieces to represent a chess board
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class GameBoard {
	
	public Piece[][]board = new Piece[8][8]; //change from String to chessPiece when created

	
	/**
	 * Constructs an empty GameBoard
	 */
	public GameBoard() {
		
	 		
	}
	
	
	/**
	 * Checks whether or not the space is empty
	 * @param board Board being played on
	 * @param y int representing file
	 * @param x int representing rank
	 * @return True if the space on [y][x] does not have a piece on it
	 * 
	 */
	public boolean isEmpty(GameBoard board, int y, int x) {
		if(board.board[y][x] == null) {
			return true;
		} 
		else {
			return false;
		}
	}
	
	/**
	 * Initializes pieces and locations for start of game
	 */
	public void initializeBoard() {
		board[0][0] = new Rook(true);
		board[1][0] = new Knight(true);
		board[2][0] = new Bishop(true);
		board[3][0] = new Queen(true);
		board[4][0] = new King(true);
		board[5][0] = new Bishop(true);
		board[6][0] = new Knight(true);
		board[7][0] = new Rook(true);
		for(int i = 0; i < 8; i++) {
			board[i][1] = new Pawn(true);
		}
		for(int i = 0; i < 8; i++) {
			board[i][6] = new Pawn(false);
		}
		board[0][7] = new Rook(false);
		board[1][7] = new Knight(false);
		board[2][7] = new Bishop(false);
		board[3][7] = new Queen(false);
		board[4][7] = new King(false);
		board[5][7] = new Bishop(false);
		board[6][7] = new Knight(false);
		board[7][7] = new Rook(false);
		
	}
	
	/**
	 * Prints the GameBoard
	 */
	public void printBoard() {
		
		//System.out.println();
		
		for(int i = 0; i < 8; i++) {
			
			for(int j = 0; j < 8; j++) {
				
				if(board[j][i] == null) {
					
					if((i%2 == 1 && j%2 == 0) || (i%2 == 0 && j%2 == 1)) {
						
						System.out.print("## ");
						
					}
					else {
						System.out.print("   ");
					}
					
				}
				else {
					System.out.print(board[j][i].print() + " ");
				}
				
				if(j == 7) {
					System.out.println(8-i);
				}
				
			}
			
			//System.out.println();
			
			if(i == 7) {
				System.out.println(" a  b  c  d  e  f  g  h");
			}
			
		}
		
	}

}
