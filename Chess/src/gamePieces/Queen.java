package gamePieces;

import chess.GameBoard;

/**
 * Queen represents a Queen chess piece and contains relevant methods and movement checks
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class Queen extends Piece {
	
	/**
	 * Constructs Queen object
	 * @param isBlack whether new piece is black
	 */
	public Queen(boolean isBlack) {
		super(isBlack);
	}
	
	/**
	 * Returns a string corresponding to the piece color and type
	 * @return string
	 */
	public String print() {
		
		if(isBlack) {
			return "bQ";
		}
		else {
			return "wQ";
		}
		
	}
	
	/**
	 * Determines whether the user inputed move is valid
	 * @param currentBoard current game board with piece positions
	 * @param coords array of integer coordinates corresponding to current and desired positions
	 * @return True if inputed move is allowed by rules of chess
	 */
	public boolean move(GameBoard currentBoard, int[] coords){
		
		//checks for out of bounds end coordinates
		if(coords[2] > 7 || coords[3] > 7 || coords[2] < 0 || coords[3] < 0) {
			return false;
		}
		
		//checks to see if there is a piece of the same color in the desired position
		if(currentBoard.board[coords[2]][coords[3]] != null) {
			if(currentBoard.board[coords[2]][coords[3]].isBlack == isBlack) {
				return false;
			}
		}
		
		if(coords[0] == coords[2]) {//same column
			if(coords[3] > coords[1]) {
				for(int i = 1; i < Math.abs(coords[3]-coords[1]); i++) {
					if(currentBoard.board[coords[0]][coords[1] + i] != null) {
						return false;
					}
				}
				return true;
			}
			else {
				for(int i = 1; i < Math.abs(coords[3]-coords[1]); i++) {
					if(currentBoard.board[coords[0]][coords[1]-i] != null) {
						return false;
					}
				}
				return true;
			}
		}
		else if(coords[1] == coords[3]) {//same row
			if(coords[2] > coords[0]){
				for(int i = 1; i < Math.abs(coords[2]-coords[0]); i++) {
					if(currentBoard.board[coords[0]+i][coords[1]] != null) {
						return false;
					}
				}
				return true;
			}
			else {
				for(int i = 1; i < Math.abs(coords[2]-coords[0]); i++) {
					if(currentBoard.board[coords[0]-i][coords[1]] != null) {
						return false;
					}
				}
				return true;
			}
		}
		else if(Math.abs(coords[3]-coords[1]) == Math.abs(coords[2]-coords[0])) {//diagonal cases
			
			if(coords[2] > coords[0] && coords[3] > coords[1]) {
				
				for(int i = 1; i < Math.abs(coords[2]-coords[0]); i++) {
					if(currentBoard.board[coords[0]+i][coords[1]+i] != null) {
						return false;
					}
				}
				return true;
			}
			else if(coords[2] > coords[0] && coords[3] < coords[1]){
				
				for(int i = 1; i < Math.abs(coords[2]-coords[0]); i++) {
					if(currentBoard.board[coords[0]+i][coords[1]-i] != null) {
						return false;
					}
				}
				return true;
				
			}
			else if(coords[2] < coords[0] && coords[3] > coords[1]) {
				
				for(int i = 1; i < Math.abs(coords[2]-coords[0]); i++) {
					if(currentBoard.board[coords[0]-i][coords[1]+i] != null) {
						return false;
					}
				}
				return true;
				
			}
			else {
				
				for(int i = 1; i < Math.abs(coords[2]-coords[0]); i++) {
					if(currentBoard.board[coords[0]-i][coords[1]-i] != null) {
						return false;
					}
				}
				return true;
				
			}
			
		}
		else {
			return false;
		}
		
		
	}
	
}
