package gamePieces;

import chess.GameBoard;

/**
 * Pawn represents a Pawn chess piece and contains relevant methods and movement checks
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class Pawn extends Piece{
	
	boolean firstMove = false; //for determining whether moving 2 spaces is valid
	
	/**
	 * Constructs Pawn object
	 * @param isBlack whether new piece is black
	 */
	public Pawn(boolean isBlack) {
		super(isBlack);
	}
	
	/**
	 * Returns a string corresponding to the piece color and type
	 * @return string
	 */
	public String print() {
		
		if(isBlack) {
			return "bp";
		}
		else {
			return "wp";
		}
		
	}
	
	/**
	 * Determines whether the user inputed move is valid
	 * @param currentBoard current game board with piece positions
	 * @param coords array of integer coordinates corresponding to current and desired positions
	 * @return True if inputed move is allowed by rules of chess
	 */
	public boolean move(GameBoard currentBoard, int[] coords){
		
		//checks for out of bounds end coordinates
		if(coords[2] > 7 || coords[3] > 7 || coords[2] < 0 || coords[3] < 0) {
			return false;
		}
		
		if(isBlack == true) {	
			
			if(coords[0] == coords[2] && coords[3]-coords[1] == 2 && firstMove == false) {
				
				if((currentBoard.board[coords[0]][coords[1]+1] == null) && (currentBoard.board[coords[2]][coords[3]] == null)) {
					firstMove = true;
					return true;
				}
				else {
					System.out.println("took this false");
					return false;
				}
				
			}
			else if(coords[0] == coords[2] && coords[3]-coords[1] == 1) {
				
				if(currentBoard.board[coords[2]][coords[3]] == null) {
					firstMove = true;
					return true;
				}
				else {
					return false;
				}
			}
			else if(((coords[0] == coords[2] +1) || (coords[0] == coords[2] -1)) && (coords[3] - coords[1] == 1)) {
				
				if(currentBoard.board[coords[2]][coords[3]] != null) {
					if(currentBoard.board[coords[0]][coords[1]].isBlack != currentBoard.board[coords[2]][coords[3]].isBlack) {
						firstMove = true;
						return true;
					}
					else {
						return false;
					}
				}
				//en passant condition for black
				else if((currentBoard.board[coords[2]][coords[3]-1] != null) && coords[1] == 4) {
					if(currentBoard.board[coords[2]][coords[3]-1].print().charAt(1) == 'p') {
						firstMove = true;					
						currentBoard.board[coords[2]][coords[3]-1] = null;
						return true;
					} else {
						return false;
					}
				}
				else {
					return false;
				}
				
			}
			
		}
		else {
			
			if(coords[0] == coords[2] && coords[1]-coords[3] == 2 && firstMove == false) {
				
				if((currentBoard.board[coords[0]][coords[1]-1] == null) && (currentBoard.board[coords[2]][coords[3]] == null)) {
					firstMove = true;
					return true;
				}
				else {
					return false;
				}
				
			}
			else if(coords[0] == coords[2] && coords[1]-coords[3] == 1) {
				
				if(currentBoard.board[coords[2]][coords[3]] == null) {
					firstMove = true;
					return true;
				}
				else {
					return false;
				}
				
			}
			else if(((coords[0] == coords[2] +1) || (coords[0] == coords[2] -1)) && (coords[1] - coords[3] == 1)) {
				
				if(currentBoard.board[coords[2]][coords[3]] != null) {
					if(currentBoard.board[coords[0]][coords[1]].isBlack != currentBoard.board[coords[2]][coords[3]].isBlack) {
						firstMove = true;
						return true;
					}
					else {
						return false;
					}
				}
				//en passant condition for black
				else if((currentBoard.board[coords[2]][coords[3]+1] != null) && coords[1] == 3) {
					if(currentBoard.board[coords[2]][coords[3]+1].print().charAt(1) == 'p') {
						firstMove = true;					
						currentBoard.board[coords[2]][coords[3]+1] = null;
						return true;
					} else {
						return false;
					}
				} else {
					return false;
				}
				
			}
			
		}

		return false;
	}
	
}
