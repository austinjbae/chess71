package gamePieces;

import chess.GameBoard;

/**
 * Knight represents a Knight chess piece and contains relevant methods and movement checks
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class Knight extends Piece {
	
	/**
	 * Constructs Knight object
	 * @param isBlack whether new piece is black
	 */
	public Knight(boolean isBlack) {
		super(isBlack);
	}
	
	/**
	 * Returns a string corresponding to the piece color and type
	 * @return string
	 */
	public String print() {
		
		if(isBlack) {
			return "bN";
		}
		else {
			return "wN";
		}
		
	}
	
	/**
	 * Determines whether the user inputed move is valid
	 * @param currentBoard current game board with piece positions
	 * @param coords array of integer coordinates corresponding to current and desired positions
	 * @return True if inputed move is allowed by rules of chess
	 */
	public boolean move(GameBoard currentBoard, int[] coords) {
		
		if(coords[2] > 7 || coords[3] > 7 || coords[2] < 0 || coords[3] < 0) {
			return false;
		}
		
		//checks to see if there is a piece of the same color in the desired position
		if(currentBoard.board[coords[2]][coords[3]] != null) {
			if(currentBoard.board[coords[2]][coords[3]].isBlack == isBlack) {
				return false;
			}
		}
		
		if(Math.abs(coords[2] - coords[0]) == 1 && Math.abs(coords[3] - coords[1]) == 2) {
			return true;
		}
		else if(Math.abs(coords[2] - coords[0]) == 2 && Math.abs(coords[3] - coords[1]) == 1) {
			return true;
		}
		else {
			return false;
		}
		
		
	}

}
