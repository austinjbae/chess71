package gamePieces;

import chess.GameBoard;

/**
 * King represents a King chess piece and contains relevant methods and movement checks
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public class King extends Piece {
	
	/**
	 * Constructs King object
	 * @param isBlack whether new piece is black
	 */
	public King(boolean isBlack) {
		super(isBlack);
	}
	
	/**
	 * Returns a string corresponding to the piece color and type
	 * @return string
	 * 
	 */
	public String print() {
		
		if(isBlack) {
			return "bK";
		}
		else {
			return "wK";
		}
		
	}
	
	/**
	 * Determines whether the user inputed move is valid
	 * @param currentBoard current game board with piece positions
	 * @param coords array of integer coordinates corresponding to current and desired positions
	 * @return True if inputed move is allowed by rules of chess
	 */
	public boolean move(GameBoard currentBoard, int[] coords) {
		
		if(coords[2] > 7 || coords[3] > 7 || coords[2] < 0 || coords[3] < 0) {
			return false;
		}
		
		//checks to see if there is a piece of the same color in the desired position
		if(currentBoard.board[coords[2]][coords[3]] != null) {
			if(currentBoard.board[coords[2]][coords[3]].isBlack == isBlack) {
				return false;
			}
		}
		
		//up-down
		if(coords[0] == coords[2] && Math.abs(coords[3] - coords[1]) == 1) {
			return true;
		}
		//left-right
		else if(coords[1] == coords[3] && Math.abs(coords[2] - coords[0]) == 1) {
			return true;
		}
		//castle
		else if(coords[1] == coords[3] && Math.abs(coords[2] - coords[0]) == 2) {
			//right castle
			if(coords[0] - coords[2] < 0) {
				//checks if king and rook have moved
				if((currentBoard.board[4][coords[1]] != null) && (currentBoard.board[7][coords[1]] != null)) {
					if(currentBoard.board[4][coords[1]].print().charAt(1) == 'K' && currentBoard.board[7][coords[1]].print().charAt(1) == 'R') {
						if(currentBoard.isEmpty(currentBoard, 5, coords[1]) && currentBoard.isEmpty(currentBoard, 6, coords[1])) {
							return true;
						} else {return false;}
					} else {return false;}
				} else {return false;}
			} 
			//left castle
			else {
				//checks if king and rook have moved
				if((currentBoard.board[4][coords[1]] != null) && (currentBoard.board[7][coords[1]] != null)) {
					if(currentBoard.board[4][coords[1]].print().charAt(1) == 'K' && currentBoard.board[7][coords[1]].print().charAt(1) == 'R') {
						if(currentBoard.isEmpty(currentBoard, 1, coords[1]) && currentBoard.isEmpty(currentBoard, 2, coords[1]) && 
								currentBoard.isEmpty(currentBoard, 3, coords[1])) {
							return true;
						} else {return false;}
					} else {return false;}
				} else {return false;}
			}
		}
		//diag
		else if(Math.abs(coords[2] - coords[0]) == 1 && Math.abs(coords[3] - coords[1]) == 1) {
			return true;
		}
		else {
			return false;
		}
		
	}

}
