package gamePieces;
import chess.GameBoard;

/**
 * Piece is an abstract class to allow for polymorphic method calls on chess pieces
 * @author Austin Bae (ajb384), Ian Martin (ihm12)
 *
 */
public abstract class Piece {
	boolean isBlack;
	
	/**
	 * Class Constructor specifying the color of the piece
	 * @param isBlack whether piece is black
	 * 
	 */
	public Piece(boolean isBlack) {
		this.isBlack = isBlack;
	}
	
	/**
	 * Returns a true or false depending on whether or not the piece's move is valid given a game board and coordinates.
	 * The indexing for file and rank go from 0-7, with y representing file and x representing rank. This method does not move the piece.
	 * @param board the game board object
	 * @param coords an integer array containing translated coordinates of the start and end position in format [start y, start x, end y, end x]
	 * @return true if the move is valid, false if not
	 * 
	 */
	public abstract boolean move(GameBoard board, int[] coords);
	
	/**
	 * Print function to print the color and type of this piece
	 * @return Color and type of this piece in format "wp","bK","wN"...
	 * 
	 */
	public abstract String print();
	
	/**
	 * Returns piece's'isBlack' field
	 * @return piece's 'isBlack' field, a boolean representing whether or not this piece is black
	 * 
	 */
	public boolean getColor() {
		return isBlack;
	}
}
